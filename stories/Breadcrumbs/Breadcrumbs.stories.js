import MyBreadcrumbs from './BreadCrumbs.vue'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
	title: 'Example/Breadcrumbs',
	component: MyBreadcrumbs,
	// More on argTypes: https://storybook.js.org/docs/vue/api/argtypes
	argTypes: {
		color: {
			control: { type: 'select' },
			options: [
				undefined,
				'secondary',
				'success',
				'warning',
				'danger',
				'light',
				'dark',
			],
		},
		size: {
			control: { type: 'select' },
			options: [undefined, 'very small', 'small', 'large', 'very large'],
		},
		type: {
			control: { type: 'select' },
			options: [undefined, 'shadow', 'inverted', 'outlined'],
		},
		divider: {
			control: { type: 'select' },
			options: [undefined, 'chevron', 'pipe', 'whitespace'],
		},
	},
}

const items = [
	{
		text: 'Home',
		href: '#',
		disabled: false,
	},
	{
		text: 'Data',
		href: '#',
		disabled: false,
	},
	{
		text: 'Library',
		href: '#',
		disabled: true,
	},
]

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args) => ({
	// Components used in your story `template` are defined in the `components` object
	components: { MyBreadcrumbs },
	// The story's `args` need to be mapped into the template through the `setup()` method
	setup() {
		return { args }
	},
	// And then the `args` are bound to your component with `v-bind="args"`
	template: '<my-breadcrumbs v-bind="args" />',
})

export const Standard = Template.bind({})
// More on args: https://storybook.js.org/docs/vue/writing-stories/args
Standard.args = {
	label: 'Breadcrumbs',
	items: items,
}

export const Shadow = Template.bind({})
Shadow.args = {
	label: 'Breadcrumbs',
	type: 'shadow',
	items: items,
}

export const Inverted = Template.bind({})
Inverted.args = {
	label: 'Breadcrumbs',
	type: 'inverted',
	items: items,
}

export const Outlined = Template.bind({})
Outlined.args = {
	label: 'Breadcrumbs',
	type: 'outlined',
	items: items,
}
